use std::{net::SocketAddr, path::PathBuf};

use axum::{
    extract::{
        ws::{Message, WebSocket},
        ConnectInfo, State, WebSocketUpgrade,
    },
    response::IntoResponse,
    routing::get,
    Error, Router,
};
use lobby::LobbyManagerHandle;
use player::PlayerManagerHandle;
use tower_http::services::ServeDir;

mod lobby;
mod player;

#[derive(Clone)]
struct AppState {
    player_manager: PlayerManagerHandle,
    lobby_manager: LobbyManagerHandle,
}

#[tokio::main]
async fn main() {
    let player_manager = player::PlayerManagerHandle::new();
    let lobby_manager = lobby::LobbyManagerHandle::new();
    let _ = lobby_manager.create_lobby().await;

    let static_content_dir = PathBuf::from("static-content");

    let app = Router::new()
        .fallback_service(ServeDir::new(static_content_dir).append_index_html_on_directories(true))
        .route("/ws", get(websocket_initialiser))
        .with_state(AppState {
            player_manager,
            lobby_manager,
        });

    let listener = tokio::net::TcpListener::bind("127.0.0.1:8080")
        .await
        .unwrap();
    axum::serve(
        listener,
        app.into_make_service_with_connect_info::<SocketAddr>(),
    )
    .await
    .unwrap();
}

async fn websocket_initialiser(
    State(app_state): State<AppState>,
    websocket_upgrade: WebSocketUpgrade,
    ConnectInfo(addr): ConnectInfo<SocketAddr>,
) -> impl IntoResponse {
    println!("connection from {}", addr.to_string());

    websocket_upgrade.on_upgrade(move |websocket| {
        websocket_handler(websocket, app_state.player_manager, app_state.lobby_manager)
    })
}

async fn websocket_handler(
    mut websocket: WebSocket,
    player_manager: PlayerManagerHandle,
    lobby_manager: LobbyManagerHandle,
) {
    println!("oh nice, websocket initialised");
    //TODO: register websocket somewhere
    let player_id = player_manager.register_player().await;

    lobby_manager.add_player_to_lobby(player_id, 0).await;

    // spawn actor for websocket handling
    tokio::spawn(async move {
        loop {
            tokio::select! {
                Some(request) = websocket.recv() => {
                    handle_request(request);

                    if let Err(err) = websocket.send(Message::Text("testi".into())).await {
                        println!("ooh: {err}");
                    }
                },
                else => break,
            }
        }
        println!("websocket connection has been closed");
    });
}

fn handle_request(request: Result<Message, Error>) {
    match request {
        Ok(request_message) => {
            println!("got message: {request_message:?}");
        }
        Err(err) => println!("receive error: {err}"),
    }
}
