struct Lobby {
    id: usize,
    players: Vec<usize>,
}

struct LobbyManager {
    receiver: tokio::sync::mpsc::Receiver<LobbyManagerMessage>,
    counter: usize,
    lobbies: Vec<Lobby>,
}

enum LobbyManagerMessage {
    CreateLobby {
        response: tokio::sync::oneshot::Sender<usize>,
    },
    AddPlayerToLobby {
        player_id: usize,
        lobby_id: usize,
        response: tokio::sync::oneshot::Sender<bool>,
    },
}

impl LobbyManager {
    fn new(receiver: tokio::sync::mpsc::Receiver<LobbyManagerMessage>) -> Self {
        Self {
            receiver,
            counter: 0,
            lobbies: Vec::new(),
        }
    }

    fn handle_message(&mut self, message: LobbyManagerMessage) {
        match message {
            LobbyManagerMessage::CreateLobby { response } => {
                let id = self.counter;
                self.counter += 1;

                let lobby = Lobby {
                    id,
                    players: Vec::new(),
                };
                self.lobbies.push(lobby);

                let _ = response.send(id);
            }
            LobbyManagerMessage::AddPlayerToLobby {
                player_id,
                lobby_id,
                response,
            } => {
                for lobby in &mut self.lobbies {
                    if lobby.id == lobby_id {
                        if !lobby.players.contains(&player_id) {
                            lobby.players.push(player_id);
                        }
                        let _ = response.send(true);
                        return;
                    }
                }
                let _ = response.send(false);
            }
        }
    }

    async fn run(&mut self) {
        while let Some(message) = self.receiver.recv().await {
            self.handle_message(message);
        }
    }
}

#[derive(Clone)]
pub struct LobbyManagerHandle {
    sender: tokio::sync::mpsc::Sender<LobbyManagerMessage>,
}

const QUEUE_SIZE: usize = 64;

impl LobbyManagerHandle {
    pub fn new() -> Self {
        let (sender, receiver) = tokio::sync::mpsc::channel(QUEUE_SIZE);
        let mut lobby_manager = LobbyManager::new(receiver);
        tokio::spawn(async move { lobby_manager.run().await });

        Self { sender }
    }

    pub async fn create_lobby(&self) -> usize {
        let (sender, receiver) = tokio::sync::oneshot::channel();
        let message = LobbyManagerMessage::CreateLobby { response: sender };

        let _ = self.sender.send(message).await;
        receiver.await.expect("LobbyManager has been killed")
    }

    pub async fn add_player_to_lobby(&self, player_id: usize, lobby_id: usize) -> bool {
        let (sender, receiver) = tokio::sync::oneshot::channel();
        let message = LobbyManagerMessage::AddPlayerToLobby {
            player_id,
            lobby_id,
            response: sender,
        };

        let _ = self.sender.send(message).await;
        receiver.await.expect("LobbyManager has been killed")
    }
}
