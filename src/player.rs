struct Player {
    id: usize,
    lobby: Option<usize>,
}

struct PlayerManager {
    receiver: tokio::sync::mpsc::Receiver<PlayerManagerMessage>,
    counter: usize,
    players: Vec<Player>,
}

enum PlayerManagerMessage {
    CreatePlayer {
        response: tokio::sync::oneshot::Sender<usize>,
    },
}

impl PlayerManager {
    fn new(receiver: tokio::sync::mpsc::Receiver<PlayerManagerMessage>) -> Self {
        Self {
            receiver,
            counter: 0,
            players: Vec::new(),
        }
    }

    fn handle_message(&mut self, message: PlayerManagerMessage) {
        match message {
            PlayerManagerMessage::CreatePlayer { response } => {
                let id = self.counter;
                self.counter += 1;

                let player = Player { id, lobby: None };
                self.players.push(player);

                let _ = response.send(id);
            }
        }
    }

    async fn run(&mut self) {
        while let Some(message) = self.receiver.recv().await {
            self.handle_message(message);
        }
    }
}

#[derive(Clone)]
pub struct PlayerManagerHandle {
    sender: tokio::sync::mpsc::Sender<PlayerManagerMessage>,
}

const QUEUE_SIZE: usize = 64;

impl PlayerManagerHandle {
    pub fn new() -> Self {
        let (sender, receiver) = tokio::sync::mpsc::channel(QUEUE_SIZE);
        let mut player_manager = PlayerManager::new(receiver);
        tokio::spawn(async move { player_manager.run().await });

        Self { sender }
    }

    pub async fn register_player(&self) -> usize {
        let (sender, receiver) = tokio::sync::oneshot::channel();
        let message = PlayerManagerMessage::CreatePlayer { response: sender };

        let _ = self.sender.send(message).await;
        receiver.await.expect("PlayerManager has been killed")
    }
}
